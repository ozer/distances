import math
def manhattan_distance(u, v):
    """
    Computes the Manhattan distance (Taxicab distance) between two vectos `u` and `v`.
    
    The Manhattan distance between `u` and `v`, is defined as:

    \sum_{i=1}^{N}|u_i – v_i|

    Parameters
    ----------
    u : list
        Input vector.
    v : list
        Input vector.

    Returns
    -------
    manhattan : double
        The Manhattan distance between vectors `u` and `v`.
    """
    distance = 0
    for u_i, v_i in zip(u, v):
        distance += abs(u_i - v_i)
        
    return distance