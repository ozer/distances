import distances

def test_euclidean_example():
    assert distances.euclidean_distance([2,-1],[-2,2]) == 5
